package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.entity.Session;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    void close(@NotNull final Session session);

    void closeAll(@NotNull final Session session);

    @Nullable
    User getUser(@NotNull final Session session);

    @NotNull
    String getUserId(@NotNull final Session session);

    @NotNull
    List<Session> findAll(@NotNull Session session);

    @Nullable
    Session sign(@NotNull Session session);

    boolean isValid(@NotNull final Session session);

    void validate(@NotNull final Session session);

    void validate(@NotNull final Session session, @Nullable Role role);

    @Nullable
    Session open(@NotNull final String login, @NotNull final String password);

    boolean checkDataAccess(@Nullable final String login, @Nullable final String password);

    void signOutByLogin(@Nullable final String login);

    void signOutByUserId(@Nullable final String userId);

}
